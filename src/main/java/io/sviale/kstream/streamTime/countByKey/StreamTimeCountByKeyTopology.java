package io.sviale.kstream.streamTime.countByKey;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.state.Stores;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.TimeZone;

public class StreamTimeCountByKeyTopology {

    public static StreamsBuilder getWindowTopology() {

        var streamsBuilder = new StreamsBuilder();

        var bananaStream =
                streamsBuilder.stream("BANANA", Consumed.with(Serdes.String(), Serdes.String()))
                        .groupByKey()
                        .windowedBy(TimeWindows.ofSizeWithNoGrace(Duration.ofHours(1)))
                        .count(Materialized.as("store-count"))
                        .toStream()
                        .map((Windowed<String> key, Long count) ->
                                new KeyValue<String, String>(windowedKeyToString(key), count.toString()));

        bananaStream.to("BANANA_ALERT", Produced.with(Serdes.String(), Serdes.String()));

        return streamsBuilder;
    }
    
    public static StreamsBuilder getWindowWithGracePeriodTopology() {

        var streamsBuilder = new StreamsBuilder();

        var bananaStream =
                streamsBuilder.stream("BANANA", Consumed.with(Serdes.String(), Serdes.String()))
                        .groupByKey()
                        .windowedBy(TimeWindows.ofSizeAndGrace(Duration.ofHours(1), Duration.ofMinutes(10)))
                        .count(Materialized.as("store-count"))
                        .toStream()
                        .map((Windowed<String> key, Long count) ->
                                new KeyValue<String, String>(windowedKeyToString(key), count.toString()));

        bananaStream.to("BANANA_ALERT", Produced.with(Serdes.String(), Serdes.String()));

        return streamsBuilder;
    }
    
    public static StreamsBuilder getTopologyWithSuppress() {

        StreamsBuilder streamsBuilder = new StreamsBuilder();
        
        var bananaStream =
                streamsBuilder.stream("BANANA", Consumed.with(Serdes.String(), Serdes.String()))
                        .groupByKey()
                        .windowedBy(TimeWindows.ofSizeAndGrace(Duration.ofHours(1), Duration.ofMinutes(10)))
                        .count(Materialized.as("store-count"))
                        .suppress(Suppressed.untilWindowCloses(Suppressed.BufferConfig.unbounded()))
                        .filter((k, v) -> "miguel".equals(k.key()))
                        .toStream()
                        .map((Windowed<String> key, Long count) ->
                                new KeyValue<String, String>(windowedKeyToString(key), count.toString()));

        bananaStream.to("BANANA_ALERT", Produced.with(Serdes.String(), Serdes.String()));

        return streamsBuilder;
    }
    
    public static StreamsBuilder getTopologyWithStreamTimeTranformer() {

        StreamsBuilder streamsBuilder = new StreamsBuilder();

        var countStore = Stores.timestampedKeyValueStoreBuilder(
                Stores.persistentTimestampedKeyValueStore("COUNT_STORE"),
                Serdes.String(),
                Serdes.Long());
        streamsBuilder.addStateStore(countStore);

        var bananaStream =
                streamsBuilder.stream("BANANA", Consumed.with(Serdes.String(), Serdes.String()))
                        .transform(CountWithStreamTimeTransformer::new, "COUNT_STORE");

        bananaStream.to("BANANA_ALERT", Produced.with(Serdes.String(), Serdes.String()));

        return streamsBuilder;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    public static StreamsBuilder getTopologyWithWallClockTimeTranformer() {

        StreamsBuilder streamsBuilder = new StreamsBuilder();

        var countStore = Stores.timestampedKeyValueStoreBuilder(
                Stores.persistentTimestampedKeyValueStore("COUNT_STORE"),
                Serdes.String(),
                Serdes.Long());
        streamsBuilder.addStateStore(countStore);

        var bananaStream =
                streamsBuilder.stream("BANANA", Consumed.with(Serdes.String(), Serdes.String()))
                        .transform(CountWithWallClockTimeTransformer::new, "COUNT_STORE");

        bananaStream.to("BANANA_ALERT", Produced.with(Serdes.String(), Serdes.String()));

        return streamsBuilder;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    private static String windowedKeyToString(Windowed<String> key) {

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ssZZZZ");
        sdf.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
        return String.format("%s from %s to %s",
                key.key(),
                sdf.format(key.window().startTime().toEpochMilli()),
                sdf.format(key.window().endTime().toEpochMilli()));
    }

}
