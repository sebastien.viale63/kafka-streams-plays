package io.sviale.kstream.streamTime.countByKey;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.PunctuationType;
import org.apache.kafka.streams.state.TimestampedKeyValueStore;
import org.apache.kafka.streams.state.ValueAndTimestamp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.TimeZone;

public class CountWithWallClockTimeTransformer implements Transformer<String, String, KeyValue<String, String>> {

    private final Logger logger = LoggerFactory.getLogger(CountWithWallClockTimeTransformer.class);

    private ProcessorContext processorContext;
    private TimestampedKeyValueStore<String, Long> countStore;

    @Override
    public void init(ProcessorContext processorContext) {
        this.processorContext = processorContext;
        this.countStore = processorContext.getStateStore("COUNT_STORE");

        this.processorContext.schedule(Duration.ofMinutes(60), PunctuationType.WALL_CLOCK_TIME, ts -> {
                    logger.info("start puntuating {}", new Date(ts));
                    try (var iterator = this.countStore.all()) {
                        while (iterator.hasNext()) {
                            var record = iterator.next();
                           logger.info("forwarding key: {}, value: {}  ", record.key , record.value.value());
                            processorContext.forward(record.key, windowedKeyToString(record, ts));
                            this.countStore.delete(record.key);
                        }
                    } catch (Exception e) {

                    }
                }
        );
    }

    @Override
    public KeyValue<String, String> transform(String key, String value) {
        if(!"banana".equals(value)){
            return null;
        }
        if (countStore.get(key) == null) {
            countStore.put(key, ValueAndTimestamp.make(1L, processorContext.timestamp()));
        } else {
            Long count = countStore.get(key).value() + 1;
            countStore.put(key, ValueAndTimestamp.make(count, processorContext.timestamp()));
        }
        return null;
    }

    @Override
    public void close() {

    }

    private String windowedKeyToString(KeyValue<String, ValueAndTimestamp<Long>> record, long ts) {

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ssZZZZ");
        sdf.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
        return String.format("%s at %s ",
                record.value.value(),
                sdf.format(ts));
    }
}
