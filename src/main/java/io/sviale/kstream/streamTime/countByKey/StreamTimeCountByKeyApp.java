package io.sviale.kstream.streamTime.countByKey;

import io.sviale.utils.PropertiesLoader;
import io.sviale.utils.StreamConstants;
import io.sviale.utils.StreamExecutionContext;
import io.sviale.utils.TopicManagement;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.errors.StreamsUncaughtExceptionHandler;
import org.apache.log4j.Logger;

public class StreamTimeCountByKeyApp {

    private static final Logger logger = Logger.getLogger(StreamTimeCountByKeyApp.class);
    
    public static void main(String[] args) {
        
        // Get stream configuration
        var streamsConfiguration = PropertiesLoader.fromYaml("application.yml");

        StreamExecutionContext.setProperties(streamsConfiguration);
        
        //Create needed topics
        TopicManagement.createTopics(StreamConstants.ORDER_APP_1_AVRO);
        TopicManagement.createTopics(StreamConstants.ORDER_APP_2_AVRO);
        TopicManagement.createTopics(StreamConstants.ORDER_APP_OUTPUT_AVRO);

        //Build topology
        var stream = new KafkaStreams(StreamTimeCountByKeyTopology.getWindowTopology().build(), streamsConfiguration);

        // Define handler in case of unmanaged exception
        stream.setUncaughtExceptionHandler((exception) -> StreamsUncaughtExceptionHandler.StreamThreadExceptionResponse.SHUTDOWN_APPLICATION);
        
        // Start stream execution
        stream.cleanUp();
        stream.start();

        // Ensure your app respond gracefully to external shutdown signal
        Runtime.getRuntime().addShutdownHook(new Thread(stream::close));
        

    }

}
