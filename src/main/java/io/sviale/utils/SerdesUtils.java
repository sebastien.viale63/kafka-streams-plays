package io.sviale.utils;

import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import org.apache.avro.specific.SpecificRecord;

import java.util.Map;

public class SerdesUtils {
    
    public static final String SCHEMA_REGISTRY_URL = "schema.registry.url";
    
    public static <T extends SpecificRecord> SpecificAvroSerde<T> getSerdes() {
        SpecificAvroSerde<T> serde = new SpecificAvroSerde();
        serde.configure(Map.of(SCHEMA_REGISTRY_URL, StreamExecutionContext.getProperties().get(SCHEMA_REGISTRY_URL)), false);
        return serde;
    }
}
