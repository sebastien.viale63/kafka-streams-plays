package io.sviale.utils;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TopicManagement {

  
    public static void createTopics(String topic) {
        Map<String, Object> config = new HashMap<>();
        config.put("bootstrap.servers", StreamExecutionContext.getProperties().get("bootstrap.servers"));
        AdminClient client = AdminClient.create(config);

        List<NewTopic> topics = new ArrayList<>();

        topics.add(new NewTopic(
                topic,
                3,
                (short) 3));

        client.createTopics(topics);
        client.close();
    }

    public static void deleteTopics(String topic) {
        Map<String, Object> config = new HashMap<>();
        config.put("bootstrap.servers", StreamExecutionContext.getProperties().get("bootstrap.servers"));
        AdminClient client = AdminClient.create(config);

        List<String> topics = new ArrayList<>();

        topics.add(topic);

        client.deleteTopics(topics);
        client.close();
    }


}
