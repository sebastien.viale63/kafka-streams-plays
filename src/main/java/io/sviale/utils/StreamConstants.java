package io.sviale.utils;


/**
 * Contains all the names of the topics and state stores
 */
public class StreamConstants {
    
    public final static String ORDER_APP_1_AVRO = "ORDER_APP_1_AVRO";

    public final static String ORDER_APP_2_AVRO = "ORDER_APP_2_AVRO";

    public final static String ORDER_APP_OUTPUT_AVRO = "ORDER_APP_OUTPUT_AVRO";

    public final static String ORDER_LINE_APP_1_REPARTITION_AVRO = "ORDER_LINE_APP_1_REPARTITION_AVRO";

    public final static String ORDER_LINE_APP_2_REPARTITION_AVRO = "ORDER_LINE_APP_2_REPARTITION_AVRO";
    
    public static final String STATE_STORE_APP1_ORDER= "STATE_STORE_APP1_ORDER";
    
    public static final String STATE_STORE_APP2_ORDER= "STATE_STORE_APP2_ORDER";
}
