package io.sviale.utils;

import java.util.Map;
import java.util.Properties;

public class StreamExecutionContext {

    private static Properties properties;
    
    public StreamExecutionContext() {
    }

    public static Properties getProperties() {
        return properties;
    }

    public static void setProperties(Properties properties) {
        StreamExecutionContext.properties = properties;
    }
}
