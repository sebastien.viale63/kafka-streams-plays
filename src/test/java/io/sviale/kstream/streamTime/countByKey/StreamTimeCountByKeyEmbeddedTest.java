package io.sviale.kstream.streamTime.countByKey;

import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import io.sviale.utils.PropertiesLoader;
import io.sviale.utils.StreamExecutionContext;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.integration.utils.EmbeddedKafkaCluster;
import org.apache.kafka.streams.integration.utils.IntegrationTestUtils;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class StreamTimeCountByKeyEmbeddedTest {

    public static EmbeddedKafkaCluster CLUSTER = new EmbeddedKafkaCluster(1);

    private final static String registryUrl = "mock://url";

    private final static String bananaTopic = "BANANA";
    private final static String bananaAlertTopic = "BANANA_ALERT";
    private static AdminClient admin = null;

    @BeforeAll
    public static void StartCluster() throws IOException, InterruptedException {
        System.setProperty("java.io.tmpdir", "/temp/kafka");
    }

    @AfterEach
    public void shutdown() {
        CLUSTER.stop();
    }

    @BeforeEach
    public void start() throws IOException, InterruptedException {
        CLUSTER.start();
        CLUSTER.createTopic(bananaTopic, 3, 1);
        CLUSTER.createTopic(bananaAlertTopic, 3, 1);
    }

   // TODO

    private static Properties getStreamConfig() {
        var streamsConfiguration = PropertiesLoader.fromYaml("application.yml");
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, CLUSTER.bootstrapServers());
        streamsConfiguration.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        streamsConfiguration.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        streamsConfiguration.put("topic.min.insync.replicas", "1");
        streamsConfiguration.put("replication.factor", "1");
    //    streamsConfiguration.put("schema.registry.url", registryUrl);
        streamsConfiguration.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);
        return streamsConfiguration;
    }

    private static Properties getProducerConfig() {
        final Properties producerConfig = new Properties();
        producerConfig.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, CLUSTER.bootstrapServers());
        producerConfig.put(ProducerConfig.ACKS_CONFIG, "all");
        producerConfig.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        producerConfig.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
    //    producerConfig.put("schema.registry.url", registryUrl);
        return producerConfig;
    }

    private static Properties getConsumerConfig() {
        final Properties consumerConfig = new Properties();
        consumerConfig.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, CLUSTER.bootstrapServers());
        consumerConfig.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        consumerConfig.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, LongDeserializer.class);
        consumerConfig.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        consumerConfig.put(ConsumerConfig.GROUP_ID_CONFIG, "test_group");
        consumerConfig.put(KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG, false);
    //    consumerConfig.put("schema.registry.url", registryUrl);
        return consumerConfig;
    }
    
}
