package io.sviale.kstream.streamTime.countByKey;

import io.sviale.utils.PropertiesLoader;
import io.sviale.utils.StreamExecutionContext;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.*;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class StreamTimeCountByKeyTest {

    private final Logger logger = LoggerFactory.getLogger(StreamTimeCountByKeyTest.class);


    private final String bananaTopic = "BANANA";
    private final String bananaAlertTopic = "BANANA_ALERT";

    private TestInputTopic<String, String> bananaInputTopic;
    private TestOutputTopic<String, String> bananaOutPutAlertTopic;

    @Test
    @Order(1)
    public void windowTopologyTest() {

        try (final var testDriver = init(StreamTimeCountByKeyTopology.getWindowTopology())) {

            loadDatas();

            final var actualOutputs = bananaOutPutAlertTopic.readKeyValuesToList();
            logger.info("");
            for (var record : actualOutputs) {
                logger.info(record.key + " == " + record.value);
            }
            logger.info("");
        }

    }

    @Test
    @Order(2)
    public void windowWithGracePeriodTopologyTest() {

        try (final var testDriver = init(StreamTimeCountByKeyTopology.getWindowWithGracePeriodTopology())) {

            loadDatas();

            final var actualOutputs = bananaOutPutAlertTopic.readKeyValuesToList();
            logger.info("");
            for (var record : actualOutputs) {
                logger.info(record.key + " == " + record.value);
            }
            logger.info("");
        }
    }

    @Test
    @Order(3)
    public void windowWithSuppressApiTopologyTest() {

        try (final var testDriver = init(StreamTimeCountByKeyTopology.getTopologyWithSuppress())) {
            
            loadDatas();
         
            final var actualOutputs = bananaOutPutAlertTopic.readKeyValuesToList();
            logger.info("");
            for (var record : actualOutputs) {
                logger.info(record.key + " == " + record.value);
            }
            logger.info("");
        }
    }

    @Test
    @Order(4)
    public void windowWithSuppressApiTopologyTest_1() {

        try (final var testDriver = init(StreamTimeCountByKeyTopology.getTopologyWithSuppress())) {

            loadDatas();
            bananaInputTopic.pipeInput("advance", "advance", getDate(16, 11, 0));

            final var actualOutputs = bananaOutPutAlertTopic.readKeyValuesToList();
            logger.info("");
            for (var record : actualOutputs) {
                logger.info(record.key + " == " + record.value);
            }
            logger.info("");
        }
    }

    @Test
    @Order(5)
    public void transformerWithStreamTimeTopologyTest() {

        try (final var testDriver = init(StreamTimeCountByKeyTopology.getTopologyWithStreamTimeTranformer())) {
            
            loadDatas();

            final var actualOutputs = bananaOutPutAlertTopic.readKeyValuesToList();
            logger.info("");
            for (var record : actualOutputs) {
                logger.info(record.key + " == " + record.value);
            }
            logger.info("");
        }
    }

    @Test
    @Order(6)
    public void transformerWithStreamTimeTopologyTest_1() {

        try (final var testDriver = init(StreamTimeCountByKeyTopology.getTopologyWithStreamTimeTranformer())) {

            loadDatas();
            bananaInputTopic.pipeInput("advance", "advance", getDate(16, 0, 0));

            final var actualOutputs = bananaOutPutAlertTopic.readKeyValuesToList();
            logger.info("");
            for (var record : actualOutputs) {
                logger.info(record.key + " == " + record.value);
            }
            logger.info("");
        }
    }

    @Test
    @Order(7)
    public void transformerWithWallClockTimeTopologyTest() {

        try (final var testDriver = init(StreamTimeCountByKeyTopology.getTopologyWithWallClockTimeTranformer())) {

            loadDatasWithAdvanceClock(testDriver);

            final var actualOutputs = bananaOutPutAlertTopic.readKeyValuesToList();
            logger.info("");
            for (var record : actualOutputs) {
                logger.info(record.key + " == " + record.value);
            }
            logger.info("");
        }
    }
    
    private TopologyTestDriver init(StreamsBuilder topology) {

   //     logger.info("topology: {}", topology.build().describe());
        
        final var streamsConfiguration = PropertiesLoader.fromYaml("application.yml");

        StreamExecutionContext.setProperties(streamsConfiguration);
        TopologyTestDriver testDriver = new TopologyTestDriver(topology.build(), streamsConfiguration, getDriverDate());

        bananaInputTopic = testDriver
                .createInputTopic(bananaTopic, Serdes.String().serializer(), Serdes.String().serializer());
        bananaOutPutAlertTopic = testDriver
                .createOutputTopic(bananaAlertTopic, Serdes.String().deserializer(), Serdes.String().deserializer());
        
        return testDriver;

    }

    private void loadDatas() {

        // 1.10 pm
        bananaInputTopic.pipeInput("miguel", "banana", getDate(13, 10, 0));
        // 1.40 pm
        bananaInputTopic.pipeInput("miguel", "banana", getDate(13, 40, 0));
        // 2.05 pm
        bananaInputTopic.pipeInput("miguel", "banana", getDate(14, 5, 0));
        // 1.50 pm
        bananaInputTopic.pipeInput("miguel", "banana", getDate(13, 50, 0));
        // 2.40 pm
        bananaInputTopic.pipeInput("miguel", "banana", getDate(14, 40, 0));
        // 2.55 pm
        bananaInputTopic.pipeInput("miguel", "banana", getDate(14, 55, 0));
        // 3.10 pm
        bananaInputTopic.pipeInput("miguel", "banana", getDate(15, 10, 0));
        // 3.30 pm
        bananaInputTopic.pipeInput("miguel", "banana", getDate(15, 30, 0));

    }

    protected Instant getDriverDate() {
        LocalDateTime localDateTime = LocalDateTime.now().withHour(13).withMinute(00).withSecond(00);
        return ZonedDateTime.of(localDateTime, ZoneId.systemDefault()).toInstant();
       // return Instant.ofEpochMilli(1577836800000L);
    }

    private void loadDatasWithAdvanceClock(TopologyTestDriver testDriver) {

        // 1.10 pm
        bananaInputTopic.pipeInput("miguel", "banana", getDate(13, 10, 0));
        // 1.40 pm
        bananaInputTopic.pipeInput("miguel", "banana", getDate(13, 40, 0));
        // advance clock time of one hour
        testDriver.advanceWallClockTime(Duration.ofHours(1));
        // 2.05 pm
        bananaInputTopic.pipeInput("miguel", "banana", getDate(14, 5, 0));
        // 1.50 pm
        bananaInputTopic.pipeInput("miguel", "banana", getDate(13, 50, 0));
        // 2.40 pm
        bananaInputTopic.pipeInput("miguel", "banana", getDate(14, 40, 0));
        // 2.55 pm
        bananaInputTopic.pipeInput("miguel", "banana", getDate(14, 55, 0));
        // advance clock time of one hour
        testDriver.advanceWallClockTime(Duration.ofHours(1));
        // 3.10 pm
        bananaInputTopic.pipeInput("miguel", "banana", getDate(15, 10, 0));
        // 3.30 pm
        bananaInputTopic.pipeInput("miguel", "banana", getDate(15, 30, 0));
        // advance clock time of one hour
        testDriver.advanceWallClockTime(Duration.ofHours(1));

    }

    private long getDate(int hour, int minute, int second) {
        LocalDateTime localDateTime = LocalDateTime.now().withHour(hour).withMinute(minute).withSecond(second);
        return ZonedDateTime.of(localDateTime, ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

}
